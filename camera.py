import cv2
import numpy as np

# 定义两个圆的直径大小
d_small = 200
d_big = 400

cap = cv2.VideoCapture(0)

while True:
    ret, pic = cap.read()

    # 获取图片宽高，判断定义的圆的直径是否超过了图片的短边，如果超过则说明图片或输入参数不符合要求，此时退出程序
    pic_height = pic.shape[0]
    pic_width = pic.shape[1]

    if pic_height > pic_width:
        max_diameter = pic_height
    else:
        max_diameter = pic_width

    err_happen = False
    if d_big > max_diameter:
        err_happen = True

    if err_happen:
        print('请更换图片或修改圆的大小')
        break
    else:
        cv2.circle(pic, (int(pic_width / 2), int(pic_height / 2)), int(d_small / 2), (255, 255, 0), 1, 8, 0)
        cv2.circle(pic, (int(pic_width / 2), int(pic_height / 2)), int(d_big / 2), (255, 0, 255), 1, 8, 0)

        # 创建一张张4通道的新图片（矩阵，存储类型为uint8），包含透明通道，初始化为全透明，并将其复制给img_small、img_small用于处理
        img_new = np.zeros((pic_height, pic_width, 4), np.uint8)
        img_new[:, :, 0:3] = pic[:, :, 0:3]
        img_small = img_big = img_new

        # 创建一张4通道的图片，大小为原图尺寸，用于制作圆的mask
        img_circle = np.zeros((pic_height, pic_width, 4), np.uint8)
        img_circle[:, :, 3] = 0  # 设置为全透明

        # 创建遮罩
        img_big_mask = cv2.circle(img_circle, (int(pic_width / 2), int(pic_height / 2)), int(d_big / 2),
                                  (255, 255, 255),
                                  -1)  # 设置大内圆不透明
        img_big_mask_not = cv2.bitwise_not(img_big_mask)

        # alpha融合后才可以保存（矩阵相加，为不需要的部分设置全透明）
        # img_big[:, :, 3] = img_big_mask[:, :, 0]
        # cv2.imwrite("big.png", img_big)

        # 旋转
        rotateMat = cv2.getRotationMatrix2D((int(pic_width / 2), int(pic_height / 2)), 45, 1)
        img_big_rotate = cv2.warpAffine(img_big, rotateMat, (pic_width, pic_height))

        # 应用遮罩
        img_big_out = cv2.add(img_new, img_big_mask)
        img_big = cv2.add(img_big_rotate, img_big_mask_not)

        # 叠加图像，因为遮罩外是白色，所以减去255恰好恢复图像
        img_res_big = img_big_out + img_big - 255

        # 显示旋转后的图
        cv2.imshow('Big Outside', img_big_out)
        cv2.imshow('Big Circle', img_big)
        cv2.imshow('Big Result', img_res_big)

        # 创建一张4通道的图片，大小为原图尺寸，用于制作圆的mask
        img_circle = np.zeros((pic_height, pic_width, 4), np.uint8)
        img_circle[:, :, 3] = 0  # 设置为全透明

        # 创建遮罩
        img_small_mask = cv2.circle(img_circle, (int(pic_width / 2), int(pic_height / 2)), int(d_small / 2),
                                    (255, 255, 255), -1)  # 设置小内圆不透明
        img_small_mask_not = cv2.bitwise_not(img_small_mask)

        # alpha融合后才可以保存（矩阵相加，为不需要的部分设置全透明）
        # img_small[:, :, 3] = img_small_mask[:, :, 0]
        # cv2.imwrite("small.png", img_small)

        # 旋转
        rotateMat = cv2.getRotationMatrix2D((int(pic_width / 2), int(pic_height / 2)), -45, 1)
        img_small_rotate = cv2.warpAffine(img_small, rotateMat, (pic_width, pic_height))

        # 应用遮罩
        img_small_out = cv2.add(img_res_big, img_small_mask)
        img_small = cv2.add(img_small_rotate, img_small_mask_not)

        # 叠加图像，因为遮罩外是白色，所以减去255恰好恢复图像
        img_res_small = img_small_out + img_small - 255

        cv2.imshow('Small Outside', img_small_out)
        cv2.imshow('Small Circle', img_small)
        cv2.imshow('Small Result', img_res_small)

    if cv2.waitKey(1) & 0xFF == ord(' '):
        break

cap.release()
cv2.destroyAllWindows()
